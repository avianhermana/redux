import React from 'react';
import {View, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HistoryScreen from '../screen/HistoryScreen';
import HomeScreen from '../screen/HomeScreen';
import ProfileScreen from '../screen/ProfileScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeStack from './HomeStack';

const Tab = createBottomTabNavigator();

export default function MainNavigator(){
    return(
        <Tab.Navigator
                screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Home') {
                    iconName = focused ? 'home' : 'home-outline';
                    } else if (route.name === 'History') {
                    iconName = focused ? 'menu' : 'menu-outline';
                    } else if (route.name === 'Profile') {
                    iconName = focused ? 'person' : 'person-outline';
                    }

                    // You can return any component that you like here!
                    return (
                    <View>
                        <Ionicons name={iconName} size={size} color={color} />
                    </View>
                    );
                },
                })}
                tabBarOptions={{
                activeTintColor: 'blue',
                inactiveTintColor: 'gray',
                activeBackgroundColor: 'lightblue',
                inactiveBackgroundColor: 'lightgray',
                }}
            >
            <Tab.Screen name="History" component={HistoryScreen}/>
            <Tab.Screen name="Home" component={HomeStack}/>
            <Tab.Screen name="Profile" component={ProfileScreen}/>
        </Tab.Navigator>
    );
}