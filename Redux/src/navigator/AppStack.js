import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screen/HomeScreen';
import MoreScreen from '../screen/MoreScreen';
import LoginScreen from '../screen/LoginScreen';
import MainNavigator from './MainNavigator';
import EditProfileScreen from '../screen/EditProfileScreen';

const Stack = createStackNavigator();

export default function AppStack() {
    return(
        <Stack.Navigator>
            {/* <Stack.Screen name="Login" component={LoginScreen}/> */}
            <Stack.Screen options={{headerShown:false}} name="Main" component={MainNavigator}/>
            <Stack.Screen name="More" component={MoreScreen}/>
            <Stack.Screen name="EditProfile" component={EditProfileScreen}/>
        </Stack.Navigator>
    );
}