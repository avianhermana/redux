import React, { useEffect } from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {apiGetProfileDetail} from '../assets/Profile';

function HomeScreen(props){
    useEffect(() => {
        apiGetProfileDetail('id')
        .then((res) => {
            props.updateProfile(res.data);
        })
        .catch((err)=> {
            console.error(err);
        });
    },[]);

    return(
        <View>
            <Text>Hi, {props.nameLengkap}</Text>

            <TouchableOpacity onPress={() => props.navigation.navigate('Detail')}>
                <Text style={{color:'red', fontSize:24}}> Go to detail </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.navigation.navigate('More')}>
                <Text style={{color:'red', fontSize:24}}> More </Text>
            </TouchableOpacity>
        </View>
    );
}

const mapStateToProps = (state) => ({
    nameLengkap : state.profile.fullname,
});

const mapDispatchToProps = (dispatch) => ({
    updateProfile:(data) => dispatch({type:'UPDATE_PROFILE',
    payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);