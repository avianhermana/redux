import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export default function MoreScreen(props){
    return(
        <View>
            <Text>Home</Text>

            <TouchableOpacity onPress={() => props.navigation.navigate('Detail')}>
                <Text style={{color:'red', fontSize:24}}> Go to detail </Text>
            </TouchableOpacity>
        </View>
    );
}