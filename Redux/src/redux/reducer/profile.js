
const initialState ={
    id: null,
    username: null,
    fullname: null,
    email: null,
    bio: null,
};

const profile = (state = initialState, action) => {
    switch (action.type){
        case 'UPDATE_PROFILE': {
            return {
                ...state,
                ...action.payload,
            };
        }
        default:
            return state;
    }
};

export default profile;