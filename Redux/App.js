import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import NewsScreen from './src/screen/NewsScreen';
import MainNavigator from './src/navigator/MainNavigator';
import AppStack from './src/navigator/AppStack';
import ProfileScreen from './src/screen/ProfileScreen';
import {Provider} from 'react-redux';
import store from './src/store';

function App(){
  return (
  <Provider store={store}>
    <NavigationContainer>
      <AppStack/>
      {/* <ProfileScreen/> */}
    </NavigationContainer>
  </Provider>
  // <NewsScreen/>
  );
}

export default App;